# STRATIO WARS DECRYPTOR #

This Repository contains a Scala REST API whose purpose is to decrypt the Death Star messages intercepted by Stratio.

## Requirements

* Java Software Developer's Kit (SE) 1.8 or higher
* sbt 1.3.4 or higher.

To check your Java version, enter the following in a command window:

```bash
java -version
```

To check your sbt version, enter the following in a command window:

```bash
sbt sbtVersion
```

If you do not have the required versions, follow these links to obtain them:

* [Java SE](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* [sbt](http://www.scala-sbt.org/download.html)

## Start HTTP Service

Service will be run in localhost, at port 25984 *(is the number of stormtroopers destined for the first death star :laughing:)*

Run the project with sbt in your favorite terminal:

```bash
sbt run
```

## Usage

### Decrypt a single message

You can decrypt a single message directly in your browser, using the ```/decrypt/``` route followed by the message:

```
http://127.0.0.1:25984/decrypt/2952410b-0a94-446b-8bcb-448dc6e30b08
```

Or using ```curl```:

```bash
curl http://localhost:25984/decrypt/2952410b-0a94-446b-8bcb-448dc6e30b08
```

Getting resulting decryption in json format:

```javascript
{"galaxy":34,"planet":"edcb86430","quadrant":10,"starSystem":42}%                                                                       
```

If server can't decrypt message, you'll get an error:

```bash
curl http://localhost:25984/decrypt/no-valid-message
```

```
Cannot decrypt message 'no-valid-message'
```

### Decrypt multiples messages

You can decrypt a multiples messages with ```curl```, using the ```/decrypt/``` route with a json list:

```bash
curl -H "Content-type: application/json" -X POST -d '["2952410b-0a94-446b-8bcb-448dc6e30b08", "no-valid-message"]' http://localhost:25984/decrypt
```
The result is a json as follows:

```javascript
[{"decryption":{"galaxy":34,"planet":"edcb86430","quadrant":10,"starSystem":42},"inputMessage":"2952410b-0a94-446b-8bcb-448dc6e30b08"},{"error":"Unknown encryption","inputMessage":"no-valid-message"}]
```

Decrypted messages results in an object with input message and its decryption:

```javascript
{
    "inputMessage": "2952410b-0a94-446b-8bcb-448dc6e30b08",
    "decryption": {
        "galaxy":34,
        "planet":"edcb86430",
        "quadrant":10,
        "starSystem":42
    }
}
```

Messages that can't be decrypted results in an object with input message and an error:

```javascript
{
    "inputMessage": "no-valid-message",
    "error": "Unknown encryption"
}
```