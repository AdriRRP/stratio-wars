package com.stratio.decryptor

import com.stratio.decryptor.catalog.Coordinate
import org.scalatest.funsuite.AnyFunSuite

class DecryptorTest extends AnyFunSuite {
  val positiveTest = Map(
    "2952410b-0a94-446b-8bcb-448dc6e30b08" ->
      Coordinate(
        planet = "edcb86430",
        galaxy = 34,
        quadrant = 10,
        starSystem = 42
      ),
    "6f9c15fa-ef51-4415-afab-36218d76c2d9" ->
      Coordinate(
        planet = "dc9876321",
        galaxy = 73,
        quadrant = 15,
        starSystem = 46
      ),
    "2ab81c9b-1719-400c-a676-bdba976150eb" ->
      Coordinate(
        planet = "edba976510",
        galaxy = 64,
        quadrant = 9,
        starSystem = 35
      ),
  )

  positiveTest.foreach { case (msg, result) =>
    test(s"Decrypting $msg expecting $result") {
      assert(Decryptor(msg) match {
        case Some(coordinate) => coordinate == result
        case None => false
      })
    }
  }

  val unsupportedMessageTest = List(
    "",
    "unsuported message",
    "2952410b-0a94-446b-8bcb-448dc6e30b08 ",
    null
  )

  unsupportedMessageTest.foreach(msg =>
    test(s"Decrypting unsupported message '$msg' expecting None") {
      assert(Decryptor(msg).isEmpty)
    })

}