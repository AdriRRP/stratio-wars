package com.stratio.restapi

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, HttpRequest, MessageEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.stratio.decryptor.catalog.Coordinate
import com.stratio.restapi.catalog.{CoordinateBucket, Success, Unknown}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import spray.json.DefaultJsonProtocol
import spray.json._


class DecryptorRoutesTest  extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest {
  lazy val testKit = ActorTestKit()
  implicit def typedSystem = testKit.system
  override def createActorSystem(): akka.actor.ActorSystem =
    testKit.system.classicSystem

  val decryptorRegistry = testKit.spawn(DecryptorRegistry())
  lazy val routes = new DecryptorRoutes(decryptorRegistry).decryptorRoutes

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
  import JsonFormats._
  import DefaultJsonProtocol._

  "DecryptorRoutes" should {

    "return error if can't decrypt message (GET /users)" in {

      val request = HttpRequest(uri = "/decrypt/no-valid-message")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        contentType should ===(ContentTypes.`text/plain(UTF-8)`)

        entityAs[String] should ===("Cannot decrypt message 'no-valid-message'")
      }
    }

    "return valid decryption when message is decrypted (GET /users)" in {

      val request = HttpRequest(uri = "/decrypt/2ab81c9b-1719-400c-a676-bdba976150eb")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        contentType should ===(ContentTypes.`application/json`)

        entityAs[Coordinate] should ===(Coordinate(
          planet = "edba976510",
          galaxy = 64,
          quadrant = 9,
          starSystem = 35
        ))
      }
    }


    "return CoordinateBucket list when post a json with list of coordinates (POST /decrypt)" in {

      val messages = List(
        "2ab81c9b-1719-400c-a676-bdba976150eb",
        "no-valid-message"
      )

      val msgEntity = Marshal(messages).to[MessageEntity].futureValue

      val request = Post(uri = "/decrypt").withEntity(msgEntity)

      val expectedJson =
        ("""[{"decryption":{"galaxy":64,"planet":"edba976510","quadrant":9,"starSystem":35},""" +
        """"inputMessage":"2ab81c9b-1719-400c-a676-bdba976150eb"},""" +
        """{"error":"Unknown encryption","inputMessage":"no-valid-message"}]""").parseJson

      request ~> routes ~> check {

        status should ===(StatusCodes.OK)

        contentType should ===(ContentTypes.`application/json`)

        println(entityAs[String])

        entityAs[JsValue] should ===(expectedJson)
      }
    }
  }
}
