package com.stratio.restapi

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import com.stratio.decryptor.Decryptor
import com.stratio.decryptor.catalog.Coordinate
import com.stratio.restapi.catalog.{CoordinateBucket, Success, Unknown}

object DecryptorRegistry {

  def apply(): Behavior[Command] = Behaviors.receiveMessage {
    case Decrypt(message, replyTo) =>
      replyTo ! DecryptResponse(Decryptor(message = message))
      Behaviors.same
    case DecryptBucket(messageBucket, replyTo) =>
      replyTo ! DecryptBucketResponse(messageBucket.map(message => Decryptor(message = message) match {
        case None => Unknown(inputMessage = message)
        case Some(coordinate) => Success(
          inputMessage = message,
          decryption = coordinate
        )
      }))
      Behaviors.same
  }

  sealed trait Command

  final case class Decrypt(message: String, replyTo: ActorRef[DecryptResponse]) extends Command

  final case class DecryptBucket(messageBucket: List[String], replyTo: ActorRef[DecryptBucketResponse]) extends Command

  final case class DecryptResponse(maybeCoordinate: Option[Coordinate])

  final case class DecryptBucketResponse(maybeCoordinateBucket: List[CoordinateBucket])

}
