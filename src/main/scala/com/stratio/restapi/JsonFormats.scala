package com.stratio.restapi

import com.stratio.decryptor.catalog.Coordinate
import com.stratio.restapi.catalog.{CoordinateBucket, Success, Unknown}
import spray.json.{DefaultJsonProtocol, JsValue, JsonWriter, enrichAny}

object JsonFormats {

  import DefaultJsonProtocol._

  implicit val coordinateJsonFormat = jsonFormat4(Coordinate)
  implicit val coordinateBucketUnknownJsonFormat = jsonFormat2(Unknown)
  implicit val coordinateBucketSuccessJsonFormat = jsonFormat2(Success)
  implicit val actionFormat = lift(new JsonWriter[CoordinateBucket] {
    override def write(obj: CoordinateBucket): JsValue = obj match {
      case a: Unknown => a.toJson
      case b: Success => b.toJson
    }
  })

}
