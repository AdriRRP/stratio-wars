package com.stratio.restapi

import akka.actor.typed.{ActorRef, ActorSystem}
import akka.actor.typed.scaladsl.AskPattern._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import com.stratio.restapi.DecryptorRegistry.{Decrypt, DecryptBucket, DecryptBucketResponse, DecryptResponse}
import spray.json.DefaultJsonProtocol

import scala.concurrent.Future

class DecryptorRoutes(decryptorRegistry: ActorRef[DecryptorRegistry.Command])(implicit val system: ActorSystem[_]) {

  import JsonFormats._
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  private implicit val timeout = Timeout.create(system.settings.config.getDuration("stratio-wars.routes.ask-timeout"))
  val decryptorRoutes: Route =
    pathPrefix("decrypt") {
      concat(
        path(Segment) { message =>
          concat(
            get {
              rejectEmptyResponse {
                onSuccess(decrypt(message = message)) { response =>
                  complete(response.maybeCoordinate match {
                    case None => s"Cannot decrypt message '$message'"
                    case other => other
                  })
                }
              }
            }
          )
        },
        pathEnd {
          concat(
            post {
              import DefaultJsonProtocol._
              entity(as[List[String]]) { messageBucket =>
                rejectEmptyResponse {
                  onSuccess(decryptBucket(messageBucket = messageBucket)) { response =>
                    complete(response.maybeCoordinateBucket)
                  }
                }
              }
            }
          )
        }
      )
    }

  def decrypt(message: String): Future[DecryptResponse] =
    decryptorRegistry.ask(Decrypt(message = message, _))

  def decryptBucket(messageBucket: List[String]): Future[DecryptBucketResponse] =
    decryptorRegistry.ask(DecryptBucket(messageBucket = messageBucket, _))
}
