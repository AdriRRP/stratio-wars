package com.stratio.restapi.catalog

import com.stratio.decryptor.catalog.Coordinate

sealed trait CoordinateBucket

case class Unknown(
                    inputMessage: String,
                    error: String = "Unknown encryption"
                  ) extends CoordinateBucket

case class Success(
                    inputMessage: String,
                    decryption: Coordinate
                  ) extends CoordinateBucket
