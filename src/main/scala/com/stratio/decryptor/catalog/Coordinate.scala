package com.stratio.decryptor.catalog

final case class Coordinate(
                             planet: String,
                             galaxy: Int,
                             quadrant: Int,
                             starSystem: Int
                           ) {
  @Override
  override def toString: String = s"$galaxy-$quadrant-$starSystem-$planet"
}
