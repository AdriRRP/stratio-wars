package com.stratio.decryptor

import com.stratio.decryptor.catalog.Coordinate

import scala.util.matching.Regex

object Decryptor {
  val encryptedMessagePattern: Regex =
    """([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})""".r

  def apply(message: String): Option[Coordinate] =
    message.toLowerCase() match {
      case encryptedMessagePattern(galaxy, quadrant, starSystem1, starSystem2, planet) =>
        Some(Coordinate(
          planet = planet.distinct.sorted.reverse,
          galaxy = galaxy.grouped(1).map(c => Integer.parseInt(c, 16)).sum,
          quadrant = quadrant.grouped(1).map(c => Integer.parseInt(c, 16)).max,
          starSystem = {
            val pairs = starSystem1.grouped(1).map(c => Integer.parseInt(c, 16)) zip
              starSystem2.grouped(1).map(c => Integer.parseInt(c, 16))
            pairs.map { case (a, b) => Math.max(a, b) }.sum
          }
        ))
      case _ => None
    }

}
